package com.equiporojo.imediadav;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

/**
 * 
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class NewMediaReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, final Intent intent) {
		Log.d(CONSTANTS.TAG_LOG, "received pic or video intent: "+intent.getAction());
		
		final boolean isNewPic = android.hardware.Camera.ACTION_NEW_PICTURE.equals(intent.getAction())||intent.getAction().compareTo("com.android.camera.NEW_PICTURE")==0;
		final boolean isNewVid = android.hardware.Camera.ACTION_NEW_VIDEO.equals(intent.getAction())||intent.getAction().compareTo("com.android.camera.NEW_VIDEO")==0;

		if (!isNewPic && !isNewVid) return;

		final SharedPreferences preferences = context.getSharedPreferences(CONSTANTS.PAQUETE_PREFERENCIAS,
				Context.MODE_PRIVATE);

		if (isNewPic && !preferences.getBoolean("auto_sync_camera_pictures", true)) {
			Log.d(CONSTANTS.TAG_LOG, "automatic camera picture sync is disabled, ignoring");
			return;
		}

		if (isNewVid && !preferences.getBoolean("auto_sync_camera_videos", true)) {
			Log.d(CONSTANTS.TAG_LOG, "automatic camera video sync is disabled, ignoring");
			return;
		}

		final boolean syncOnWifiOnly = preferences.getBoolean("auto_sync_on_wifi_only", true);

		Log.d(CONSTANTS.TAG_LOG, "New picture was taken");
		final Uri uri = intent.getData();
		Log.d(CONSTANTS.TAG_LOG, "picture uri = " + uri);

		final ConnectivityManager cs = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo info = cs.getActiveNetworkInfo();
		if (info == null) {
			return;
		}

		// If we have WIFI connectivity, upload immediately
		final boolean isWifi = info.isConnected() && (ConnectivityManager.TYPE_WIFI == info.getType());
		if (!syncOnWifiOnly || isWifi) {
			Log.d(CONSTANTS.TAG_LOG, "Trying to upload " + uri + " immediately (on WIFI)");
			final Intent ulIntent = new Intent(context, UploadService.class);
			ulIntent.putExtra(Intent.EXTRA_STREAM, uri);
			context.startService(ulIntent);
		} else {
			Log.d(CONSTANTS.TAG_LOG, "Queueing " + uri + "for later (not on WIFI)");
			// otherwise, queue the image for later			
			new IMediaDavOpenHelper(context).queueUri(uri);
		}
	}
}