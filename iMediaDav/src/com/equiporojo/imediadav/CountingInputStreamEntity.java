// Based on code from Ben Hardill:
// http://www.hardill.me.uk/wordpress/?p=646
package com.equiporojo.imediadav;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.http.entity.InputStreamEntity;

/**
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class CountingInputStreamEntity extends InputStreamEntity {
	private UploadListener listener;
	private long length;

	public CountingInputStreamEntity(final InputStream instream,final long length) {
		super(instream, length);
		this.length = length;
	}

	public void setUploadListener(final UploadListener listener) {
		this.listener = listener;
	}

	@Override
	public void writeTo(final OutputStream outstream) throws IOException {
		super.writeTo(new CountingOutputStream(outstream));
	}

	final class CountingOutputStream extends OutputStream {
		private long counter = 0l;
		private int lastPercent = 0;
		private OutputStream outputStream;

		public CountingOutputStream(final OutputStream outputStream) {
			this.outputStream = outputStream;
		}

		@Override
		public void write(final int oneByte) throws IOException {
			this.outputStream.write(oneByte);
			this.counter++;
			if (CountingInputStreamEntity.this.listener != null) {
				int percent = (int) ((this.counter * 100) / CountingInputStreamEntity.this.length);
				// NB: We need to call this only when the percentage actually
				// changed, otherwise updating the notification will churn
				// through memory far too quickly.
				if (this.lastPercent != percent) {
					CountingInputStreamEntity.this.listener.onChange(percent);
					this.lastPercent = percent;
				}
			}
		}
	}

	public interface UploadListener {
		public void onChange(final int percent);
	}
}