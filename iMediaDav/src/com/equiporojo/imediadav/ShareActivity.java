package com.equiporojo.imediadav;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;

/**
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class ShareActivity extends Activity {

	/*
	 * Takes one or multiple images (see AndroidManifest.xml) and calls
	 * shareImageWithUri() on each one.
	 *
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);
		finish();

		final Intent intent = getIntent();
		final String action = intent.getAction();
		final String type = intent.getType();

		if (type == null)
			return;

		if (!intent.hasExtra(Intent.EXTRA_STREAM))
			return;

		if (Intent.ACTION_SEND.equals(action)) {
			final Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
			shareImageWithUri(imageUri);
		} else if (Intent.ACTION_SEND_MULTIPLE.equals(action)) {
			final ArrayList<Parcelable> list = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
			for (final Parcelable p : list) {
				shareImageWithUri((Uri) p);
			}
		}
	}

	private void shareImageWithUri(final Uri uri) {
		Log.d(CONSTANTS.TAG_LOG, "Sharing " + uri.toString());

		final Intent ulIntent = new Intent(this, UploadService.class);
		ulIntent.putExtra(Intent.EXTRA_STREAM, uri);
		startService(ulIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.share, menu);
		return true;
	}
}