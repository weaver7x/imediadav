package com.equiporojo.imediadav;

import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;

// XXX: I am aware of the fact that the EditTextPreferences don't display a
// meaningful summary. This seems to be possible, see
// http://stackoverflow.com/questions/531427/how-do-i-display-the-current-value-of-an-android-preference-in-the-preference-su
// However, I think it is overly complicated and I'm not motivated to test
// this app on a tablet. Patches welcome.

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 * 
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;
	
	private static final String KEY_PASS_OCULTA = "****";
	
	private EditTextPreference webdavUrlPreference;
	private EditTextPreference webdavUserPreference;
	private EditTextPreference webdavPwdPreference;
		

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostCreate(final Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		setupSimplePreferencesScreen();
		
		this.webdavUrlPreference = (EditTextPreference) getPreferenceScreen().findPreference(CONSTANTS.KEY_URL);
		this.webdavUserPreference = (EditTextPreference) getPreferenceScreen().findPreference(CONSTANTS.KEY_USER);
		this.webdavPwdPreference = (EditTextPreference) getPreferenceScreen().findPreference(CONSTANTS.KEY_PASSWORD);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		super.onPause();
		
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();
		
		final SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
		this.webdavUrlPreference.setSummary(sharedPreferences.getString(CONSTANTS.KEY_URL, CONSTANTS.KEY_VACIO));
		this.webdavUserPreference.setSummary(sharedPreferences.getString(CONSTANTS.KEY_USER, CONSTANTS.KEY_VACIO));
		if (sharedPreferences.getString(CONSTANTS.KEY_PASSWORD, CONSTANTS.KEY_VACIO).equals("")) {
			this.webdavPwdPreference.setSummary(CONSTANTS.KEY_VACIO);
		} else {
			this.webdavPwdPreference.setSummary(KEY_PASS_OCULTA);
		}
		
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);	
	}

	@Override
	public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
		if (key.equals(CONSTANTS.KEY_URL)) {
			this.webdavUrlPreference.setSummary(sharedPreferences.getString(CONSTANTS.KEY_URL, CONSTANTS.KEY_VACIO));
		} else if (key.equals(CONSTANTS.KEY_USER)) {
			this.webdavUserPreference.setSummary(sharedPreferences.getString(CONSTANTS.KEY_USER, CONSTANTS.KEY_VACIO));
		}
		else if (key.equals(CONSTANTS.KEY_PASSWORD)) {
			if (sharedPreferences.getString(CONSTANTS.KEY_PASSWORD, CONSTANTS.KEY_VACIO).equals("")) {
				this.webdavPwdPreference.setSummary(CONSTANTS.KEY_VACIO);
			} else {
				this.webdavPwdPreference.setSummary(KEY_PASS_OCULTA);
			}
		}
	}

	/**
	 * Shows the simplified settings UI if the device configuration if the
	 * device configuration dictates that a simplified, single-pane UI should be
	 * shown.
	 */
	@SuppressWarnings("deprecation")
	private void setupSimplePreferencesScreen() {
		if (!isSimplePreferences(this)) {
			return;
		}

		// In the simplified UI, fragments are not used at all and we instead
		// use the older PreferenceActivity APIs.

		addPreferencesFromResource(R.xml.pref_general);
		// Add 'data and sync' preferences, and a corresponding header.
		final PreferenceCategory fakeHeader = new PreferenceCategory(this);
		fakeHeader.setTitle(R.string.pref_header_autosync);
		getPreferenceScreen().addPreference(fakeHeader);
		addPreferencesFromResource(R.xml.pref_autosync);
	}

	/** {@inheritDoc} */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this) && !isSimplePreferences(this);
	}

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(final Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/**
	 * Determines whether the simplified settings UI should be shown. This is
	 * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
	 * doesn't have newer APIs like {@link PreferenceFragment}, or the device
	 * doesn't have an extra-large screen. In these cases, a single-pane
	 * "simplified" settings UI should be shown.
	 */
	private static boolean isSimplePreferences(final Context context) {
		return ALWAYS_SIMPLE_PREFS || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB || !isXLargeTablet(context);
	}

	/** {@inheritDoc} */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onBuildHeaders(final List<Header> target) {
		if (!isSimplePreferences(this)) {
			loadHeadersFromResource(R.xml.pref_headers, target);
		}
	}

	/**
	 * This fragment shows notification preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public final static class AutoSyncPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(final Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_autosync);
		}
	}
}