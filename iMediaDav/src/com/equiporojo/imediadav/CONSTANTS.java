package com.equiporojo.imediadav;

/**
 * Valores constantes y comunes a toda la aplicación.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 * 
 */
public final class CONSTANTS {
	///
	//Si cambian sus valores deben cambiarse los de pref_general.xml
	public static final String KEY_URL = "webdav_url";
	public static final String KEY_USER = "webdav_user";
	public static final String KEY_PASSWORD = "webdav_password";
	/////	
	public static final String KEY_VACIO = "";
	public static final String PAQUETE_PREFERENCIAS = "com.equiporojo.imediadav_preferences";
	public static final String TAG_LOG = "imediadav";
}