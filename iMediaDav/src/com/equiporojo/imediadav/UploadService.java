package com.equiporojo.imediadav;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;

import com.equiporojo.imediadav.CountingInputStreamEntity.UploadListener;

/**
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class UploadService extends IntentService {
	public UploadService() {
		super("UploadService");
	}

	/*
	 * Resolve a Uri like "content://media/external/images/media/9210" to an
	 * actual filename, like "IMG_20130304_181119.jpg"
	 */
	private String filenameFromUri(final Uri uri) {
		final String[] projection = { MediaColumns.DATA };
		final Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		if (cursor == null || cursor.getCount() == 0)
			return null;
		final int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();		
		return Uri.parse(cursor.getString(column_index)).getLastPathSegment().toString();
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	protected void onHandleIntent(final Intent intent) {
		final Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
		Log.d(CONSTANTS.TAG_LOG, "Uploading " + uri.toString());

		final SharedPreferences preferences = getSharedPreferences(CONSTANTS.PAQUETE_PREFERENCIAS, Context.MODE_PRIVATE);

		final String webdavUrl = preferences.getString(CONSTANTS.KEY_URL, null);
		final String webdavUser = preferences.getString(CONSTANTS.KEY_USER, null);
		String webdavPassword = preferences.getString(CONSTANTS.KEY_PASSWORD, null);
		if (webdavUrl == null) {
			Log.d(CONSTANTS.TAG_LOG, "No WebDAV URL set up.");
			return;
		}

		final ContentResolver cr = getContentResolver();

		final String filename = this.filenameFromUri(uri);
		if (filename == null) {
			Log.d(CONSTANTS.TAG_LOG, "filenameFromUri returned null");
			return;
		}

		final Builder mBuilder = new Notification.Builder(this);
		mBuilder.setContentTitle("Uploading to WebDAV server");
		mBuilder.setContentText(filename);
		mBuilder.setSmallIcon(android.R.drawable.ic_menu_upload);
		mBuilder.setOngoing(true);
		mBuilder.setProgress(100, 30, false);
		final NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);		
		mNotificationManager.notify(uri.toString(), 0, Build.VERSION.SDK_INT<16?mBuilder.getNotification():mBuilder.build());

		final HttpPut httpPut = new HttpPut(webdavUrl + filename);

		ParcelFileDescriptor fd;
		InputStream stream;
		try {
			fd = cr.openFileDescriptor(uri, "r");
			stream = cr.openInputStream(uri);			
		} catch (final FileNotFoundException e1) {
			e1.printStackTrace();
			return;
		}

		final CountingInputStreamEntity entity = new CountingInputStreamEntity(stream, fd.getStatSize());
		entity.setUploadListener(new UploadListener() {
			@Override
			public void onChange(int percent) {
				mBuilder.setProgress(100, percent, false);
				Log.d(CONSTANTS.TAG_LOG,""+Build.VERSION.SDK_INT);
				mNotificationManager.notify(uri.toString(), 0, Build.VERSION.SDK_INT<16?mBuilder.getNotification():mBuilder.build());				
			}
		});

		httpPut.setEntity(entity);

		final DefaultHttpClient httpClient = new DefaultHttpClient();

		if (webdavUser != null && webdavPassword != null) {
			final AuthScope authScope = new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT);
			final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(webdavUser, webdavPassword);
			httpClient.getCredentialsProvider().setCredentials(authScope, credentials);

			try {
				httpPut.addHeader(new BasicScheme().authenticate(credentials, httpPut));
			} catch (AuthenticationException e1) {
				e1.printStackTrace();
				return;
			}
		}

		try {
			final HttpResponse response = httpClient.execute(httpPut);
			final int status = response.getStatusLine().getStatusCode();
			// 201 means the file was created.
			// 200 and 204 mean it was stored but already existed.
			if (status == 201 || status == 200 || status == 204) {
				// The file was uploaded, so we remove the ongoing notification,
				// remove it from the queue and that’s it.
				mNotificationManager.cancel(uri.toString(), 0);				
				new IMediaDavOpenHelper(this).removeUriFromQueue(uri.toString());
				return;
			}
			Log.d(CONSTANTS.TAG_LOG, "" + response.getStatusLine());
			mBuilder.setContentText(filename + ": " + response.getStatusLine());
		} catch (final ClientProtocolException e) {
			Log.d(CONSTANTS.TAG_LOG, "ClientProtocolException");
			e.printStackTrace();
			mBuilder.setContentText(filename + ": " + e.getLocalizedMessage());
		} catch (final IOException e) {
			Log.d(CONSTANTS.TAG_LOG, "IOException");
			e.printStackTrace();
			mBuilder.setContentText(filename + ": " + e.getLocalizedMessage());
		}		

		// XXX: It would be good to provide an option to try again.
		// (or try it again automatically?)
		// XXX: possibly we should re-queue the images in the database
		mBuilder.setContentTitle("Error uploading to WebDAV server");
		mBuilder.setProgress(0, 0, false);
		mBuilder.setOngoing(false);		
		mNotificationManager.notify(uri.toString(), 0, Build.VERSION.SDK_INT<16?mBuilder.getNotification():mBuilder.build());
	}
}
