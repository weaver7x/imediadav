package com.equiporojo.imediadav;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

/**
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class IMediaDavOpenHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = CONSTANTS.TAG_LOG;
	private static final String TABLE_NAME = "sync_queue";
	private static final String CAMPO_ID = "id";
	private static final String CAMPO_URI = "uri";
	private static final String CAMPO_NOT_BEFORE = "not_before";
	private static final String CAMPO_UPLOADING = "uploading";
	private static final int DATABASE_VERSION = 1;

	public IMediaDavOpenHelper(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_NAME+" ("+CAMPO_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+CAMPO_URI+" STRING NOT NULL, "+CAMPO_NOT_BEFORE+" DATETIME, "+CAMPO_UPLOADING+" BOOLEAN DEFAULT 0);");
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
	}

	/*
	 * Returns an ArrayList of Uris which are queued and not already being
	 * uploaded. Also marks all of them as being uploaded so that duplicate
	 * network change events don’t upload the same Uris a lot of times.
	 */
	@SuppressWarnings("null")
	public ArrayList<String> getQueuedUris() {
		final ArrayList<String> result = new ArrayList<String>();
		final SQLiteDatabase database = getWritableDatabase();
		database.beginTransaction();
		try {
			final Cursor cursor = database.rawQuery("SELECT "+CAMPO_URI+" FROM "+TABLE_NAME+" WHERE NOT "+CAMPO_UPLOADING, null);
			if (cursor.moveToFirst()) {
				do {
					result.add(cursor.getString(0));
				} while (cursor.moveToNext());
			}

			if (cursor != null && !cursor.isClosed())
				cursor.close();

			database.execSQL("UPDATE "+TABLE_NAME+" SET "+CAMPO_UPLOADING+" = 1");

			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		return result;
	}

	public void queueUri(final Uri uri) {		
		final ContentValues values = new ContentValues();
		values.put(CAMPO_URI, uri.toString());
		getWritableDatabase().insertOrThrow(TABLE_NAME, null, values);
	}

	public void removeUriFromQueue(final String uri) {		
		getWritableDatabase().delete(TABLE_NAME, CAMPO_URI+" = ?", new String[] { uri });
	}
}
