package com.equiporojo.imediadav;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

/**
 * 
 * Optimizado con final y con CONSTANTES.
 * @author Cristian TG <@weaver7x> <www.cristiantg.com>
 *
 */
public final class NetworkReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, final Intent intent) {		
		Log.d(CONSTANTS.TAG_LOG, "network connectivity changed");

		if (!ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction()))
			return;
		

		final NetworkInfo info = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		if (info == null || !info.isConnected()) {
			Log.d(CONSTANTS.TAG_LOG, "_NOT_ connected anymore, not doing anything.");
			return;
		}		

		if (context.getSharedPreferences(CONSTANTS.PAQUETE_PREFERENCIAS,
				Context.MODE_PRIVATE).getBoolean("auto_sync_on_wifi_only", true) && !(ConnectivityManager.TYPE_WIFI == info.getType())) {
			Log.d(CONSTANTS.TAG_LOG, "Not on WIFI, not doing anything.");
			return;
		}

		Log.d(CONSTANTS.TAG_LOG, "Checking whether pictures need to be synced");

		// XXX: It doesn't really feel right to do this blockingly in a
		// BroadcastReceiver, but I was unable to find whether this is the right
		// way or whether there is a better one.		
		final ArrayList<String> uris = new IMediaDavOpenHelper(context).getQueuedUris();
		for (final String uri : uris) {
			final Intent ulIntent = new Intent(context, UploadService.class);
			// evtl: mapintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			ulIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(uri));
			context.startService(ulIntent);
		}
	}

}
