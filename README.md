# README #

Aplicación que sube contenido multimedia a un servidor WebDAV.

Detecta automáticamente cuando se toma una nueva imagen o video con el dispositivo.

Por defecto los valores son bob/bob a un servidor fake de lab.inf.uva.es. Se configuran en res/xml/pref_general.xml

### Who do I talk to? ###

* Cristian TG @weaver7x
* Javier Alonso @javalon
* Hernán Maximiliano González @hgonzlezcaldern
* Fernando Martín @fermart